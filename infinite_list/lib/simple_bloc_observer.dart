import 'package:bloc/bloc.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print("SimpleBlocObserver.onTransition: $transition");
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    print("SimpleBlocObserver.onError $error");
    super.onError(bloc, error, stackTrace);
  }
}
